import models.Account;
import models.Customer;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer(0,"an",0);
        Customer customer2 = new Customer(1,"minh",10);
        System.out.println(customer1);
        System.out.println(customer2);

        Account account1 = new Account(0, customer1, 1000000);
        Account account2 = new Account(0, customer2,50000);
        System.out.println(account1);
        System.out.println(account2);
    }
}
